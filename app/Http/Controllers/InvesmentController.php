<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InvesmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mutualFunds()
    {
        return view('invesment.mutualfunds');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bonds()
    {
        return view('invesment.bonds');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function nps()
    {
        return view('invesment.nationalpensionscheme');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Mail;

use App\Mail\ProductInfoEmail;

use App\Product;

class ProductDetailsController extends Controller
{
    public function storeProducts(Request $request)
    {
        try {
            $this->validate($request, [
                'product_name' => 'required',
                'name' => 'required',
                'email' => 'required|email',
                'mobile' => 'required'
            ]);
    
            Mail::send('email', [
                'product_name' => $request->get('product_name'),
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'mobile' => $request->get('mobile')],
                function ($message) {
                        $message->from('chiyanmahesh01@gmail.com');
                        $message->to('mahesh.vizza@gmail.com', 'Mahesh')
                        ->subject('Testing for vizza finserv');
            });

            Product::create($request->all());
    
            return back()->with('success', 'Thanks for contacting me, I will get back to you soon!');
            
        } catch(\Exception $e) {
            dd($e);
            return response($e->getMessage(), 422);
        }
    }
    
    public function singleStoreProduct(Request $request)
    { 
        try {
            $this->validate($request, [
                'product_name' => 'required',
                'name' => 'required',
                'email' => 'required|email',
                'mobile' => 'required'
            ]);
    
            Mail::send('email', [
                'product_name' => $request->get('product_name'),
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'mobile' => $request->get('mobile')],
                function ($message) {
                        $message->from('chiyanmahesh01@gmail.com');
                        $message->to('mahesh.vizza@gmail.com', 'Mahesh')
                        ->subject('Testing for vizza finserv');
            });

            Product::create($request->all());
    
            return back()->with('success', 'Thanks for contacting me, I will get back to you soon!');
            
        } catch(\Exception $e) {
            dd($e);
            return response($e->getMessage(), 422);
        }
    
    }


    public function sendSMS()
    {  
        $mobileno = "8940850147";
        $msg = "Testing";

        $url = 'https://control.msg91.com/api/sendhttp.php?authkey=304423APds6RGy5dd26235&mobiles=91' . $mobileno . '&message=' . $msg . '&sender=VIZINS&route=4&country=0';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        return back();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

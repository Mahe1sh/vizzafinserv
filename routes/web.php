<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


 /* Loans */

 Route::get('/', 'LoansController@dashboard')->name('loans.index');
 
 Route::get('/personal-loan', 'LoansController@personal')->name('loans.personal');

 Route::get('/home-loan', 'LoansController@home')->name('loans.home');

 Route::get('/business-loan', 'LoansController@business')->name('loans.business');

 Route::get('/loan-against-property', 'LoansController@loanagainstproperty')->name('loans.loanagainstproperty'); 

 /* Credit Cards */
 
 Route::get('credit-cards', 'CardsController@index')->name('cards.index');;
 
 /* Smart Deposit */

 Route::get('fixed-deposits', 'DepositsController@fixedDeposit')->name('deposits.fixed');
 
 Route::get('recurring-deposits', 'DepositsController@recurringDeposit')->name('deposits.recurring');
 
 /* Insurance */
 
 /* Mutual Funds */
 
 Route::get('mutual-funds', 'InvesmentController@mutualFunds')->name('invesment.mutualfunds');
 
 /* Bonds */
 
 Route::get('bonds', 'InvesmentController@bonds')->name('invesment.bonds');
 
 /* NPS */
 
 Route::get('national-pension-scheme', 'InvesmentController@nps')->name('invesment.nationalpensionscheme');


Route::get('/products', 'ProductDetailsController@storeProducts');
Route::post('/products', 'ProductDetailsController@storeProducts');

Route::get('/single-product', 'ProductDetailsController@singleStoreProduct');
Route::post('/single-product', 'ProductDetailsController@singleStoreProduct');

/* SMS */

Route::get('send-sms', 'ProductDetailsController@sendSMS');

Route::get('/clear', function() {

    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
 
    return "Cleared!";
 
 });
 



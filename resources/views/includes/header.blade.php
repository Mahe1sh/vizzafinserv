 <!-- start header area -->
 <header class="header-area">
        <!-- <div class="topbar-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <div class="left-info">
                            <p><span class="fa fa-clock"></span>Opening Hours : Mon - Sat : 8.30 to 16.00</p>
                        </div>
                    </div>
                    <div class="col-md-6 text-right">
                        <div class="right-info">
                            <p><a href="#"><span><i class="fa fa-download"></i>Download Brochures</span></a> <a href="#"><span><i class="fa fa-futbol-o"></i>Support</span> </a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="logo-right-address">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="sitelogo">
                            <a href="/"><img src="asset/img/logo.svg" alt="" style="height: 100px;width: 147px;margin-top: -25px;margin-left: 9px;" /></a>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="right-address">
                            <div class="single-address">
                                <div class="icon">
                                    <img src="asset/img/call.png" alt="" />
                                </div>
                                <div class="content">
                                    <p>
                                        <span>Call Us:</span> +91 8939807558
                                    </p>
                                </div>
                            </div>
                            <div class="single-address">
                                <div class="icon">
                                    <img src="asset/img/envalope.png" alt="" />
                                </div>
                                <div class="content">
                                    <p>
                                        <span>Email us: </span> vizzafinserv@gmail.com
                                    </p>
                                </div>
                            </div>
                            <div class="single-address">
                                <a href="www.vizzafinserv.com" class="button btn btn-default btn-sm"><i class="fa fa-reply-all"></i> GET A QUOTES</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="menu-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-11 col-lg-11">
                        <div id="cssmenu">
                            <ul>
                                <li><a href="/">Loans</a>
                                    <ul>
                                        <li><a href="{{ route('loans.personal') }}">Personal Loan</a></li>
                                        <li> <a href="{{ route('loans.home') }}">Home Loan</a></li>
                                        <li> <a href="{{ route('loans.business') }}">Business Loan</a></li>
                                        <li> <a href="{{ route('loans.loanagainstproperty') }}">Loan Against Property</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ route('cards.index') }}">Credit Cards</a></li>
                                <li><a href="#">Insurance</a>
                                    <ul>
                                        <li><a href="https://vizzainsurance.com/#/health-enquiry">Health Insurance</a></li>
                                        <li><a href="https://vizzainsurance.com/#/term-enquiry">Life Insurance</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Smart Deposits</a>
                                    <ul>
                                        <li><a href="{{ route('deposits.fixed') }}">Fixed Deposits</a></li>
                                        <li><a href="{{ route('deposits.recurring') }}">Recurring Deposits</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ route('invesment.mutualfunds') }}">Mutual Funds</a></li>
                                <li><a href="{{ route('invesment.bonds') }}">Bonds</a></li>
                                <li><a href="{{ route('invesment.nationalpensionscheme') }}">NPS</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-1 text-right">
                        <div class="searchbtn">
                            <a href="#" class="fa fa-search"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="searchform">
            <input type="text" name="s" placeholder="Search Here">
            <button><i class="fa fa-times"></i></button>
        </div>
    </header>
    <!-- end of header area -->
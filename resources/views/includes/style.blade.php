<style>

@media only screen and (min-width: 1200px) {
    .container-login100 {
        margin-left: 150px !important;
    }
    .wrap-login100 {
        background-color: #f26c25 !important;
        margin-bottom: 10px !important;
    }
}

.main-slider-item:before {
    background-color: transparent !important;
}
/* Form Start */
select.form-control:not([size]):not([multiple]) {
    height: calc(4.25rem + 0px) !important;
}
.wrap-login100 {
    background-color: #f26c25 !important;
    margin-top: -150px;
}
.txt1 {
    color: white;
}
/* Form End */

.logo-right-address {
    padding: 15px 0;
}
.single-featured {
    border: none !important;
    overflow: hidden;
    margin-bottom: 30px;
}
featured .icon {
    background-color: transparent !important;
    width: 85px;
    float: left;
    text-align: center;
    line-height: 180px;
    border-right: transparent !important;
}
.single-featured .icon {
    background-color: transparent !important;
    width: 85px;
    float: left;
    text-align: center;
    line-height: 180px;
    border-right: transparent !important;
}
.single-featured .content a {
    color: #3a7cdd;
}
.single-featured .content a:hover {
    color: white !important;
}
.btn.btn-default:hover {
    background: #183d1e !important;
}
.btn.btn-default {
    background: #f26c25 !important;
}
.single-featured .content a {
    color: white !important;
}
section.get-consultation-section:before {
    background-color: white;
}

/* Tap 3 */
section.featured-section.section-padding {
    background-color: #FAF7FE;
}

/* Banner Side icon */
.active-main-slider .owl-nav .owl-next {
    border: 1px solid #f26c25! important;
    color: #f26c25 !important;
}
.active-main-slider .owl-nav .owl-prev {
    border: 1px solid #f26c25! important;
    color: #f26c25 !important;
}
.active-main-slider .owl-nav div:hover {
    background: #f26c25 !important;
    color:#f26c25 !important;
    border-color: #f26c25 !important;
}

/* Header Banner */
.page-header:before {
    background-color: transparent !important;
}
.personal-bgimg {
    position: relative;
    padding: 70px 0;
    background-image: url('../asset/img/page-header/1920x270-1.jpg');
}
.home-bgimg {
    position: relative;
    padding: 70px 0;
    background-image: url('../asset/img/page-header/1920x270-3.jpg');
}
.business-bgimg {
    position: relative;
    padding: 70px 0;
    background-image: url('../asset/img/page-header/1920x270-3.jpg');
}
.loan-against-bgimg {
    position: relative;
    padding: 70px 0;
    background-image: url('../asset/img/page-header/1920x270-4.jpg');
}
.credit-card-bgimg {
    position: relative;
    padding: 70px 0;
    background-image: url('../asset/img/page-header/1920x270-2.jpg');
}
.breadcrumb-item.active {
    color: black !important;
}
ol.breadcrumb li a {
    color: black;
}
</style>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Vizza Finance Services">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="Loans">
    <link rel="icon" href="asset/img/favicon.svg" type="image/gif">
    <title>VizzaFinServ | Home Loans, Personal Loans</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="asset/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="asset/css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="asset/css/reset.css">
    <link rel="stylesheet" type="text/css" href="asset/css/style.css">
    <link rel="stylesheet" type="text/css" href="asset/css/responsive.css">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@1,900&display=swap" rel="stylesheet">


    <!-- Form Links -->
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="form/vendor/animate/animate.css">
    <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="form/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="form/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="form/vendor/select2/select2.min.css">
    <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="form/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="form/css/util.css">
        <link rel="stylesheet" type="text/css" href="form/css/main.css">
    <!--===============================================================================================-->


    <!-- List view in css -->

    <link rel="stylesheet" type="text/css" href="asset/css/list-view.css" id="bootstrap-css">


    <!-- start footer area -->
    <section class="footer-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-4">
                    <div class="footer-widget">
                        <div class="footer-title">
                            <img src="asset/img/footer-logo.png" alt="" />
                        </div>
                        <p>Vizza Financial Services is India’s elite investment Financial Services Hypermarket engaged inter-alia in the distribution of mutual funds, loans, fixed deposits, bonds et al.</p>
                        <div class="mail">
                            <span>Any Queries :</span>reachus@vizzafinserv.com
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-2">
                    <div class="footer-widget pages-widget">
                        <div class="footer-title">
                            <h4>Products</h4>
                        </div>
                        <ul>
                            <li><a href="#">Personal Loan</a></li>
                            <li><a href="#">Home Loan</a></li>
                            <li><a href="#">Business Loan </a></li>
                            <li><a href="#">LAP </a></li>
                            <li><a href="#">Credit Cards</a></li>
                            <li><a href="#">Mutual Funds</a></li>
                            <li><a href="#">Bonds</a></li>
                            <li><a href="#">NPS</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-lg-2">
                    <div class="footer-widget pages-widget">
                        <div class="footer-title">
                            <h4>Company</h4>
                        </div>
                        <ul>
                            <li><a href="#">About us</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Privacy Policy </a></li>
                            <li><a href="#">Terms of Use </a></li>
                            <li><a href="#">Conditions</a></li>
                            <li><a href="#">Disclaimer</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <div class="footer-title">
                            <h4>Vizza Times Newszoom</h4>
                        </div>
                        <div class="form-widget">
                            <p>Subscribe to our newsletter and get latest news related to loan,credit cards,perday loan,interates rate.</p>
                            <form action="#" method="get">
                                <div class="newsletter">
                                    <label for="envalope" class="fa fa-envelope"></label>
                                    <input id="envalope" type="email" name="email" placeholder="Email address">
                                </div>
                                <button type="submit" class="button btn btn-default btn-lg">Subscribe +</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="copy-right-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-left">
                    <div class="copyright-text">
                        <p>© <i class="fa fa-copyright"></i><?php echo date("Y"); ?> <a class="text-white" href="#">Developed by TecData </a> All Rights Reserved.</p>
                    </div>
                </div>
                <div class="col-md-6 text-right">
                    <div class="footer-nave">
                        <a href="https://www.facebook.com/Vizzafinserv2021"><i class="fa fa-facebook-square"></i></a>
                        <a href="https://www.instagram.com/vizzafinserv/"><i class="fa fa-instagram"></i></a>
                        <a href="https://twitter.com/vizzafinserv"><i class="fa fa-twitter-square"></i></a>
                        <a href="https://www.linkedin.com/company/vizzafinserv"><i class="fa fa-linkedin-square"></i></a>
                        <a href="https://www.youtube.com/channel/UCLpUKpV_WrOUI_8QhgGF0zA"><i class="fa fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of footer area -->
    <!-- Optional JavaScript -->
    <script src="asset/js/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <script src="asset/js/popper.min.js"></script>
    <script src="asset/js/bootstrap.min.js"></script>
    <script src="asset/js/jquery.nice-select.js"></script>
    <script src="asset/js/menumaker.js"></script>
    <script src="asset/js/owl.carousel.min.js"></script>
    <script src="asset/js/slider.js"></script>
    <script src="asset/js/active.js"></script>


    <!-- Form Scripts -->
    <!--===============================================================================================-->
        <script src="form/vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
        <script src="form/vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
        <script src="form/vendor/daterangepicker/moment.min.js"></script>
        <script src="form/vendor/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
        <script src="form/vendor/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
        <script src="form/js/main.js"></script>

    <!-- List view in JS -->
    
    <!-- BUSINESS FORM START -->
    <div class="limiter">
        <div class="wrap-login100 p-l-60 p-r-60 p-t-42 p-b-22" style="width: auto !important; margin-top: 0px !important;">
            <form method="POST" action="/single-product">
                @csrf

                <span class="txt1">
                    <h4 style="text-align: center; font-size: 30px;"><b>You are step closer to your dream home</b></h4>
                    <input type="hidden" value="credit_cards" name="product_name">
                </span>

                <div class="p-t-31 p-b-9" style="text-align: left;">
                    <span class="txt1">
                        Name
                    </span>
                </div>
                <div class="wrap-input100 validate-input" data-validate="Name is required">
                    <input class="input100" type="text" name="name" placeholder="Enter Your Name" required />
                    <span class="focus-input100"></span>
                </div>

                <div class="p-t-31 p-b-9" style="text-align: left;">
                    <span class="txt1">
                        E-Mail
                    </span>
                </div>
                <div class="wrap-input100 validate-input" data-validate="E-Mail is required">
                    <input class="input100" type="text" name="email" placeholder="Enter Your Email" required />
                    <span class="focus-input100"></span>
                </div>

                <div class="p-t-31 p-b-9" style="text-align: left;">
                    <span class="txt1">
                        Mobile
                    </span>
                </div>
                <div class="wrap-input100 validate-input" data-validate="Mobile is required">
                    <input class="input100" type="text" name="mobile" placeholder="Enter Your Mobile" required />
                    <span class="focus-input100"></span>
                </div>

                <div class="container-login100-form-btn m-t-25">
                    <button type="submit" class="btn btn-primary btn-round login100-form-btn" style="width: auto !important;">Apply Now</button>
                </div>
            </form>
        </div>
    </div>
    <!-- FORM END -->
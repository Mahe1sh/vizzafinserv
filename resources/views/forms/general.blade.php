    <!-- GENERAL FORM START -->
    <div class="container">
        <div class="row">
            <div class="col-sm-5 col-md-6"></div>
            <div class="col-sm-5 col-md-6 offset-md-0">
                <div class="limiter">
                    <div class="container-login100">
                        <div class="row wrap-login100 p-l-60 p-r-60 p-t-42 p-b-22">
                            <form method="POST" action="/products">
                                @csrf

                                <span class="txt1">
                                    <h4 style="text-align: center;font-size: 30px;"><b>Start your Wealth Creation Journey with Vizzafinserv</b></h4>
                                </span>

                                <div class="p-t-31 p-b-9" style="text-align: left;">
                                    <span class="txt1">
                                        Product
                                    </span>
                                </div>
                                <div class="form-group wrap-input100 validate-input" data-validate="Product is required">
                                    <select class="form-control selectpicker input100" name="product_name" data-header="Select a condiment">
                                        <option data-divider="true" disabled>-- Select a Product --</option>
                                        <option data-divider="true" value="personal_loan">Personal Loan</option>
                                        <option data-divider="true" value="home_loan">Home Loan</option>
                                        <option data-divider="true" value="business_loan">Business Loan</option>
                                        <option data-divider="true" value="loan_agent_property">Loan Agent Property</option>
                                        <option data-divider="true" value="credit_cards">Credit Cards</option>
                                        <option data-divider="true" value="health_insurance">Health Insurance</option>
                                        <option data-divider="true" value="life_insurance">Life Insurance</option>
                                        <option data-divider="true" value="fixed_deposits">Fixed Deposits</option>
                                        <option data-divider="true" value="recurring_deposits">Recurring Deposits</option>
                                        <option data-divider="true" value="mutual_funds">Mutual Funds</option>
                                        <option data-divider="true" value="bonds">Bonds</option>
                                        <option data-divider="true" value="nps">NPS</option>
                                    </select>
                                    <span class="focus-input100"></span>
                                </div>

                                <div class="p-t-31 p-b-9" style="text-align: left;">
                                    <span class="txt1">
                                        Name
                                    </span>
                                </div>
                                <div class="wrap-input100 validate-input" data-validate="Name is required">
                                    <input class="input100" type="text" name="name" placeholder="Enter Your Name" required />
                                    <span class="focus-input100"></span>
                                </div>

                                <div class="p-t-31 p-b-9" style="text-align: left;">
                                    <span class="txt1">
                                        E-Mail
                                    </span>
                                </div>
                                <div class="wrap-input100 validate-input" data-validate="E-Mail is required">
                                    <input class="input100" type="text" name="email" placeholder="Enter Your Email" required />
                                    <span class="focus-input100"></span>
                                </div>

                                <div class="p-t-31 p-b-9" style="text-align: left;">
                                    <span class="txt1">
                                        Mobile
                                    </span>
                                </div>
                                <div class="wrap-input100 validate-input" data-validate="Mobile is required">
                                    <input class="input100" type="text" name="mobile" placeholder="Enter Your Mobile" required />
                                    <span class="focus-input100"></span>
                                </div>

                                <div class="container-login100-form-btn m-t-17">
                                    <button type="submit" class="btn btn-primary btn-round login100-form-btn">Send</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FORM END -->
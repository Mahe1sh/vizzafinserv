@extends('layouts.master')

@section('content')
    <!-- Start of page header -->
    <div class="personal-bgimg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 style="color: #f26c25;font-weight: bold;font-size: 35px;">Recurring Deposits</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page" style="font-size: 20px;">Small investments can lead to bigger investments. Build your savings bit by bit. </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- end of page header -->

    <section class="section-padding" style="background-color: bisque;">
        <div class="container">
            <section class="section-padding" style="padding-top: 25px; padding-bottom: 25px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">    
                            <h2 style="text-align: center; font-size: 30px; color: red;"><b>Recurring Deposit is a sort of term deposit in which you save and deposit a fixed amount at regular intervals into your Recurring Deposit account and earn a fixed rate of interest for a predefined period.</b></h2>
                        </div>
                    </div>
                </div>
            </section>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">

                    <div class="how-it-work-content">
                        <h2>Does your deposit allow you to save ANY AMOUNT  @ ANY TIME? R</h2>
                    </div><br>
                    
                        <div class="choose-img">
                            <img src="asset/img/personal-loan.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                    
                    <div class="how-it-work-content">
                        <h4><b>Let us help you to choose your Recurring Deposits.</b></h4><br>
                    </div>
                        <div class="get-in-touch">
                            @include('forms.recurringdeposit')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- start second about area -->
    <section class="second-about-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>Why Recurring Deposits ?</h2>
                        <p>We have range of product for your loan solutions</p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-between">
                <div class="col-md-6">
                    <div class="second-about-content">
                        <div class="about-cradit-list">
                            <ul>
                                <li> High interest rate</li>
                                <li> Inculcates Saving Habit</li>
                                <li> Idea for Homemakers</li>
                            </ul>
                            <a href="#" class="btn btn-default btn-sm">CONTACT US</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="about-cradit-list">
                        <ul>
                            <li> Senior Citizens</li>
                            <li> Ease of Saving</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of second about area -->

    <section class="services-page section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-12">
                    <div class="services-details">
                        <h2>Fixed Deposits with Vizzafinserv</h2>
                        <p>The following list of individuals is eligible to open a fixed deposit account in India .</p>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="inspections-tab" data-toggle="tab" href="#inspections" role="tab" aria-controls="inspections" aria-selected="true">Feature</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="mortgage-tab" data-toggle="tab" href="#mortgage" role="tab" aria-controls="mortgage" aria-selected="false">Eligibility</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="overspending-tab" data-toggle="tab" href="#overspending" role="tab" aria-controls="overspending" aria-selected="false">Benifits</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="inspections" role="tabpanel" aria-labelledby="inspections-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Minimal monthly deposits: The majority banks permit an RD account with a least monthly contribution and no boundary on maximum deposit. This is why RDs are used by a diverse range of clients, from low income groups to high net worth consumers.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Tenure Range: You can start an RD account for a pre-defined period of 1 year to 10 years.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Rate of Interest: The rate of interest for RD is much higher than Savings Account and at par with Fixed Deposits. </p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Credit Value: You can benefit easy loan against RD as lenders agree to RD account value as loan security or collateral.</p>
                            </div>
                            <div class="tab-pane fade" id="mortgage" role="tabpanel" aria-labelledby="mortgage-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Both Indian and NRI folks are entitled to open an RD account.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> The depositor can also be a corporate, company, proprietorship, commercial organization, or any government organization.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> The depositor must have a legitimate identity proof.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Besides, the account can also be opened for minor accounts. </p>Minor RD accounts below 10 years of age are started under the guardianship of parents or legal guardian.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Above 10 years of age, the minor can open an RD by providing identity proof.</p> 
                                
                            </div>
                            <div class="tab-pane fade" id="overspending" role="tabpanel" aria-labelledby="overspending-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> High interest rate - Recurring Deposits require higher interest rate than Savings Account. You can use the excess sum from your bank account and earn higher return every month.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Inculcates Saving Habit- Recurring Deposits directly instill saving habit. By sparing out a part of income every month, you learn to handle your funds meticulously every month.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Idea for Homemakers- Recurring Deposits can be in progress with negligible monthly payment and personnel with restricted income source like Homemakers and pensioners can make good use of RDs without any trouble.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Senior Citizens- Senior citizens above the age of 60 years earn a higher rate of interest on recurring deposits. So, RDs are a profitable saving scheme for old and retired people.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Ease of Saving - The ease of saving a small amount every month makes RDs easy to open and manage for all categories of individuals. A depositor is less likely to delay the monthly saving even for low income months.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- start customer faq -->
    <section class="custom-faq section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>FAQ’s</h2>
                        <p>The passages of Lorem Ipsum available but the major have suffered alteration embarrased</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div id="accordion" class="second-accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        What documents do I need to open an RD account?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    If you are previously a client of the bank, it is basically hassle-free to ask for an RD account. Just make a request online for RD and a standing instruction (SI) will be set for the tenure and amount selected by you. Or, the bank will send its agent to collect the first cheque for the same. Within 24 hours the demand will be processed, and the amount will be automatically debited from the account on the due date of every month.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        How many Recurring Deposits can a single depositor own?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    Well, as such a bank will never limit your RD accounts; however, you must know that interest earned more than 10000 on RDs attracts a tax. In case you do not have a pan card and your accounts earn taxable interest on RD, you will be charged.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="second-accordion" class="second-accordion">
                        <div class="card">
                            <div class="card-header" id="headingfour">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapsfour" aria-expanded="false" aria-controls="collapsfour">
                                        Can I withdraw the RD amount before the maturity date?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsfour" class="collapse" aria-labelledby="headingfour" data-parent="#second-accordion">
                                <div class="card-body">                 
                                    On payment of premature withdrawal charge, you can redeem your RD account. However, it is recommended not to do the same unless you have an urgent need in hand.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingfive">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsfive" aria-expanded="false" aria-controls="collapsfive">
                                        What is better- a fixed deposit or recurring deposit?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsfive" class="collapse" aria-labelledby="headingfive" data-parent="#second-accordion">
                                <div class="card-body">
                                    While you earn a competitive rate of return from RDs as compared to FDs, the maturity amount is greater in Fixed Deposit as interest is compounded quarterly. However, not everyone has a huge sum amount to reap from FDs. The suitability of the financial product is primarily adjudged based on the profile of the depositor.

                                    If you have irregular income, RD is more suitable; however, with a surplus amount you might want an FD on your name. Both are relatively safer than SIPs as they offer a fixed return on the plan.  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of customer faq -->

@stop
@extends('layouts.master')

@section('content')
    <!-- Start of page header -->
    <div class="personal-bgimg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 style="color: #f26c25;font-weight: bold;font-size: 35px;">Fixed Deposits</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page" style="font-size: 20px;">Fixed Deposits that Assures You Guaranteed Returns</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- end of page header -->

    <section class="section-padding" style="background-color: bisque;">
        <div class="container">
            <section class="section-padding" style="padding-top: 25px; padding-bottom: 25px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">    
                            <h2 style="text-align: center; font-size: 30px; color: red;"><b>Secure Your Investment with Fixed Deposits</b></h2>
                        </div>
                    </div>
                </div>
            </section>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">

                    <div class="how-it-work-content">
                        <h2>Fill the blank</h2>
                    </div><br>
                    
                        <div class="choose-img">
                            <img src="asset/img/personal-loan.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                    
                    <div class="how-it-work-content">
                        <h4><b>Let us help you to choose your Fixed Deposits.</b></h4><br>
                    </div>
                        <div class="get-in-touch">
                            @include('forms.fixeddeposit')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- start second about area -->
    <section class="second-about-area section-padding">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-6">
                    <div class="second-about-content">
                        <strong style="font-size:18px;"><b>Fixed deposits are safe investment options that guarantee trustworthy interest rates, special interest rates for senior citizens, a range of interest payment option and no market-related risks with income tax deduction. It is very important to assess the most topical fixed deposit rates in the midst of primary banks in the nation sooner than opening a new fixed deposit or renewing an existing one. </b></strong>
                        <br><br>

                        <a href="#" class="btn btn-default btn-sm">Apply Now</a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="about-cradit-list">
                        <ul>
                            <li>Fixed Deposits that Assures You Guaranteed Returns.</li>
                            <li>Assured Returns with No Market Risks.</li>
                            <li>Reliable Source of Investment.</li>
                            <li>Your Investments are our priority</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of second about area -->

        <!-- .start loan process second -->
        <section class="second-loan-process section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>How To Apply ?</h2>
                        <h3><b>Get the money in 4 simple steps<b></h3>
                    </div>
                </div>
            </div>
            <div class="row process-list">
                <div class="col-md-3 second-process">
                    <div class="second-single-loan-process">
                        <h4>Check Your Eligibility</h4>
                        <div class="icon">
                            <img src="asset/img/pade.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 second-process">
                    <div class="second-single-loan-process">
                        <h4>Fill Your Details</h4>
                        <div class="icon">
                            <img src="asset/img/dollar.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 second-process">
                    <div class="second-single-loan-process">
                        <h4>Upload Documents</h4>
                        <div class="icon">
                            <img src="asset/img/handshake.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 second-process">
                    <div class="second-single-loan-process">
                        <h4>Get your Money</h4>
                        <div class="icon">
                            <img src="asset/img/get-money.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of loan process two -->

    <section class="services-page section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-12">
                    <div class="services-details">
                        <h2>Fixed Deposits with Vizzafinserv</h2>
                        <p>The following list of individuals is eligible to open a fixed deposit account in India .</p>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="inspections-tab" data-toggle="tab" href="#inspections" role="tab" aria-controls="inspections" aria-selected="true">Feature</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="mortgage-tab" data-toggle="tab" href="#mortgage" role="tab" aria-controls="mortgage" aria-selected="false">Eligibility</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="overspending-tab" data-toggle="tab" href="#overspending" role="tab" aria-controls="overspending" aria-selected="false">Benifits</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="document-tab" data-toggle="tab" href="#document" role="tab" aria-controls="document" aria-selected="false">Documents Required</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="inspections" role="tabpanel" aria-labelledby="inspections-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Fast, easy and reliable.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Multiple payments including Net banking, UPI, NACH, and E-Mandate</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Assured 100% security and immediate call-back in case of query.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Reliable Customer Support</p>
                            </div>
                            <div class="tab-pane fade" id="mortgage" role="tabpanel" aria-labelledby="mortgage-tab">
                                <p>The following list of individuals is eligible to open a fixed deposit account in India –</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Indian resident, NRI, Individuals or joint investors (2 or more individuals), Senior citizens, Minors, Sole proprietorship, Societies or clubs, Companies, and Partnership firms.</p>
                            </div>
                            <div class="tab-pane fade" id="overspending" role="tabpanel" aria-labelledby="overspending-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Assured Returns</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Offers great flexibility</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> High Capital Appreciation</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> An additional source of Income</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Higher returns rate for Senior citizens</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Speeder growth with Compound Interest.</p>
                            </div>
                            <div class="tab-pane fade" id="document" role="tabpanel" aria-labelledby="document-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Investors need to submit certain papers regarding their identity and residential address to invest in FD schemes successfully.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Identity Proof (Any One), Passport, Voter ID card, PAN card, Aadhaar card, Driving license, Address Proof (Any One)</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Senior citizens and minor investors need to submit age proof documents as well, which includes matriculation certificate, birth certificate, etc.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- start customer faq -->
    <section class="custom-faq section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>FAQ’s</h2>
                        <p>The passages of Lorem Ipsum available but the major have suffered alteration embarrased</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div id="accordion" class="second-accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    For how much time can I invest in Fixed Deposits?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    Most Banks/NBFCs offer tenures ranging from 7 days to 10 years.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        What are the types of fixed deposits?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    There are two types of Fixed Deposit: Cumulative and Non-cumulative. In Cumulative FD, you get interest payout at maturity along with the principal amount. In Non-cumulative FD, you get interests gains periodically at regular intervals (monthly, quarterly, half-yearly and yearly) and principal amount at maturity.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="second-accordion" class="second-accordion">
                        <div class="card">
                            <div class="card-header" id="headingfour">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapsfour" aria-expanded="false" aria-controls="collapsfour">
                                        What happens to Fixed Deposits if interest rate goes up or down?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsfour" class="collapse" aria-labelledby="headingfour" data-parent="#second-accordion">
                                <div class="card-body">                 
                                    FD is locked with a specific rate; you will continue to receive that rate till maturity, even if interest rates change later. If you wish to avail the new rate, you need to invest in a new Fixed Deposit.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingfive">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsfive" aria-expanded="false" aria-controls="collapsfive">
                                        Is Fixed Deposits Taxable?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsfive" class="collapse" aria-labelledby="headingfive" data-parent="#second-accordion">
                                <div class="card-body">
                                    Yes, interest earned from FD is taxable. TDS is deducted on interest earned if it exceeds by ₹5000 per annum. However, you can submit form 15G/H to the Bank or NBFC to avoid tax deduction.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of customer faq -->

@stop
@extends('layouts.master')

@section('content')
<div class="business-bgimg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 style="color: #f26c25;font-weight: bold;font-size: 35px;">Business Loan</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('loans.business') }}">Business Loan</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Get your desired loans in just a few clicks.</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- end of page header -->

    <section class="section-padding" style="background-color: bisque;">
        <div class="container">
            <section class="section-padding" style="padding-top: 25px; padding-bottom: 25px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">    
                            <h2 style="text-align: center; font-size: 30px; color: red;"><b>Compare and Buy the Best Health Insurance Plan with Covid-19 cover</b></h2>
                        </div>
                    </div>
                </div>
            </section>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">

                    <div class="how-it-work-content">
                        <h3><b>Businessmen or entrepreneurs take business loan to meet their various business related needs such as Business Loan for Working Capital, business expansion, buying machinery or equipment or for developing exquisite infrastructure.</b></h3><br>
                    </div>
                    
                        <div class="choose-img">
                            <img src="asset/img/personal-loan.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                    
                    <div class="how-it-work-content">
                        <h4><b>Let us help you to choose your Buniess Loan.</b></h4><br>
                    </div>
                        <div class="get-in-touch">
                            @include('forms.business')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="services-page section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-12">
                    <div class="services-details">
                        <h2 style="text-align: center;">Business Loan Simple Process</h2><br>
                        <p><i class="fa fa-check-square-o" aria-hidden="true"></i> Ignite the Entrepreneur in You With a Business Loan We Ensure You Support at every step with our Business Loan.</p>
                        <p><i class="fa fa-check-square-o" aria-hidden="true"></i> Believe in fast Business Loan Disbursals with Minimum Troubles of Lengthy Paperwork.</p>
                        <p><i class="fa fa-check-square-o" aria-hidden="true"></i> We offer Customized Business Loan Solutions best suited for Your Business Requirements.</p>
                        <div class="row">
                            <div class="col-md-3 text-center">
                                <div class="single-improvement">
                                    <div class="icon">
                                        <span class="pe-7s-angle-right-circle"></span>
                                    </div>
                                    <h4>Wide range of choices.</h4>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <div class="single-improvement">
                                    <div class="icon">
                                        <span class="pe-7s-note2"></span>
                                    </div>
                                    <h4>Hassle Free Transactions.</h4>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <div class="single-improvement">
                                    <div class="icon">
                                        <span class="pe-7s-user"></span>
                                    </div>
                                    <h4>Fast, easy and reliable</h4>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <div class="single-improvement">
                                    <div class="icon">
                                        <span class="pe-7s-user"></span>
                                    </div>
                                    <h4>Reliable Customer Support</h4>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h2>Business Loan with Vizzafinserv</h2>
                        <p>Are you thinking of Availing a Business Loan Anytime Soon or Do You Need More Business Capital? Visit Vizzafinserv’s Customized Business Loan Solutions.</p>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="inspections-tab" data-toggle="tab" href="#inspections" role="tab" aria-controls="inspections" aria-selected="true">Feature</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="mortgage-tab" data-toggle="tab" href="#mortgage" role="tab" aria-controls="mortgage" aria-selected="false">Eligibility</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="overspending-tab" data-toggle="tab" href="#overspending" role="tab" aria-controls="overspending" aria-selected="false">Benifits</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="inspections" role="tabpanel" aria-labelledby="inspections-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> You can avail of this loan to tackle your immediate financial requirements like daily capital needs.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Business loans can also help in tax benefits for the company.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> You don't have to put anything as collateral or need a guarantee for a business loan up to Rs. 75,00,000.</p>
                            </div>
                            <div class="tab-pane fade" id="mortgage" role="tabpanel" aria-labelledby="mortgage-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Banks offer customized repayment schemes for business loans. You can repay the business loan Daily, Weekly or fortnightly.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> A business loan offers you a lower interest rate compared to personal loans.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> You can avail of a business loan ranging anywhere between Rs.50,000 to Rs. 75,00,000.</p>
                            </div>
                            <div class="tab-pane fade" id="overspending" role="tabpanel" aria-labelledby="overspending-tab">
                                <strong>Banks have divided applicants into two categories based on their profession. The eligibility criteria and list of documents change as according to these two categories</strong>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Self Employed Professionals (SEP): Doctors, CA, CS, architects, engineers, etc.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Self Employed Non-Professionals (SENP): Trader, commission agent, contractor, etc.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Also, if you are applying as a company, you can avail loan by applying under the following entities:</p>
                                <div class="about-cradit-list">
                                    <ul>
                                        <li>Partnerships/Propretorship</li>
                                        <li>Limited Liability Partnerships</li>
                                        <li>Private Limited Companies</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

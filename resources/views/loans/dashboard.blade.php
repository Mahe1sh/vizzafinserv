@extends('layouts.master')

@section('content')


   <!-- start header area -->
   <section class="main-slider-area">
        <div class="active-main-slider owl-carousel">
            <div class="main-slider-item" style="background-image: url(asset/img/banner_slides/Banner-1.jpg);height:800px;">
                <div class="slider-one-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="main-slider-welcome-text">
                                    <div class="slider-cell">
                                        @include('forms.general')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of header area -->
    <!-- start header bottom section -->
    <!-- <div class="header-bottom-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="single-header-bottom">
                        <div class="icon">
                            <img src="asset/img/clock.png" alt="" />
                        </div>
                        <div class="content">
                            <p>Quick & Easy Loan
                                <br> Approvals
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-header-bottom">
                        <div class="icon">
                            <img src="asset/img/user.png" alt="" />
                        </div>
                        <div class="content">
                            <p>5,000K Customers
                                <br> Satisfied
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-header-bottom">
                        <div class="icon">
                            <img src="asset/img/camera.png" alt="" />
                        </div>
                        <div class="content">
                            <p>No Prepayment or
                                <br> Hidden Fees</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-header-bottom mb0">
                        <div class="search-box">
                            <input type="text" name="s" placeholder="Enter your Zip Code">
                            <button type="submit" class="fa fa-search"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- end of header bottom section -->
    <!-- start second about area -->
    <section class="second-about-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>Why Vizza FinServ ?</h2>
                        <p>We have range of product for your loan solutions</p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-between">
                <div class="col-md-6">
                    <div class="second-about-content">
                        <div class="about-cradit-list">
                            <ul>
                                <li> Wide range of choices</li>
                                <li> We guide you through your struggles and lead you to financial security</li>
                                <li> We take responsibility for you</li>
                                <li> Hassle Free Transactions</li>
                                <li> Make an Informed Decision by discussing with our full qualified personnel</li>
                                <li> We guide you step by step for the best investment plans</li>
                            </ul>
                            <p>* Checking your rate won't affect your credit score.</p>
                            <a href="#" class="btn btn-default btn-sm">CONTACT US</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="about-cradit-list">
                        <ul>
                            <li> Complete transparency in processes</li>
                            <li> Managed by qualified and experienced fund managers</li>
                            <li> Fast, easy and reliable</li>
                            <li> Multiple payments including Net banking, UPI, NACH, and E-Mandate</li>
                            <li> Assured 100% security and immediate callback incase of query</li>
                            <li> Reliable Customer Support</li>
                            <li> Your Investments are our priority</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of second about area -->
    <!-- start featured section -->
    <section class="featured-section section-padding">
        <div class="container ">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>Loans for your life</h2>
                        <p>We have range of product for your loan solutions</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="single-featured">
                        <div class="icon">
                            <img src="asset/img/icon-1.png" alt="" />
                        </div>
                        <div class="content">
                            <h4>Personal Loan</h4>
                            <p>Pеrѕоnаl loan is unѕесurеd wіth fіxеd рауmеntѕ.</p>
                            <a href="#" class="button btn btn-default btn-sm">GET A QUOTES <i class="fa fa-angle-double-right"></i> </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="single-featured">
                        <div class="icon">
                            <img src="asset/img/car.png" alt="" />
                        </div>
                        <div class="content">
                            <h4>Home Loan</h4>
                            <p>Home loan is most popular kіnd оf installment lоаn.</p>
                            <a href="#" class="button btn btn-default btn-sm">GET A QUOTES <i class="fa fa-angle-double-right"></i> </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="single-featured">
                        <div class="icon">
                            <img src="asset/img/home.png" alt="" />
                        </div>
                        <div class="content">
                            <h4>Business Loan</h4>
                            <p>You nееd a loan tо new business expand еxіѕtіng.</p>
                            <a href="#" class="button btn btn-default btn-sm">GET A QUOTES <i class="fa fa-angle-double-right"></i> </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="single-featured">
                        <div class="icon">
                            <img src="asset/img/education.png" alt="" />
                        </div>
                        <div class="content">
                            <h4>Loan Against Property</h4>
                            <p>Buуіng a hоmе is оnе of the bіggеѕt dесіѕіоn еvеr make.</p>
                            <a href="#" class="button btn btn-default btn-sm">GET A QUOTES <i class="fa fa-angle-double-right"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of featured section -->
    <!-- start get consultation section -->
    <section class="get-consultation-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>Credit Card</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8 text-center">
                    <div class="get-consultation-content">
                        <h4 style="color: black;">Get free customer support</h4>
                        <h2 style="color: black;">HandPick the Credit Card that satisfies your needs</h2>
                        <a href="#" class="button btn btn-default btn-sm">Apply Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of get consultation section -->
    <!-- start get consultation section -->
    <section class="get-consultation-section section-padding">
        <div class="container ">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>Insurance</h2>
                        <p>We have range of product for your loan solutions</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="single-featured">
                        <div class="icon">
                            <img src="asset/img/icon-1.png" alt="" />
                        </div>
                        <div class="content">
                            <h4>Health Insurance</h4>
                            <p>Pеrѕоnаl loan is unѕесurеd wіth fіxеd рауmеntѕ.</p>
                            <a href="https://vizzainsurance.com/#/health-enquiry" class="button btn btn-default btn-sm">GET A QUOTES <i class="fa fa-angle-double-right"></i> </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="single-featured">
                        <div class="icon">
                            <img src="asset/img/car.png" alt="" />
                        </div>
                        <div class="content">
                            <h4>Life Insurance</h4>
                            <p>Home loan is most popular kіnd оf installment lоаn.</p>
                            <a href="https://vizzainsurance.com/#/term-enquiry" class="button btn btn-default btn-sm">GET A QUOTES <i class="fa fa-angle-double-right"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of get consultation section -->
@stop

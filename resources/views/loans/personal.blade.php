@extends('layouts.master')

@section('content')
    <div class="personal-bgimg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 style="color: #f26c25;font-weight: bold;font-size: 35px;">Personal Loan</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page" style="font-size: 20px;">Get your desired loans in just a few clicks.</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- end of page header -->

    <section class="section-padding" style="background-color: bisque;">
        <div class="container">
            <section class="section-padding" style="padding-top: 25px; padding-bottom: 25px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">    
                            <h2 style="text-align: center; font-size: 30px; color: red;"><b>One-stop destination for purchasing the best available Personal loans.</b></h2>
                        </div>
                    </div>
                </div>
            </section>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">

                    <div class="how-it-work-content">
                        <h2>Stop Worrying! Start Planning! Avail your personal loan through Vizza Finserv and fulfill your needs.</h2>
                    </div>
                    
                        <div class="choose-img">
                            <img src="asset/img/personal-loan.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                    
                    <div class="how-it-work-content">
                        <h4><b>Let us help you to choose your Best Personal Loan.</b></h4><br>
                    </div>
                        <div class="get-in-touch">
                            @include('forms.personal')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Status Bar working Start -->

    <!-- <section class="how-it-work-section section-padding">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <div class="how-it-work-content">
                    <div class="container">
		
        <div class="row bs-wizard" style="border-bottom:0;">
            
            <div class="col-xs-3 bs-wizard-step complete">
              <div class="text-center bs-wizard-stepnum">Step 1</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
            </div>
            
            <div class="col-xs-3 bs-wizard-step complete">
              <div class="text-center bs-wizard-stepnum">Step 2</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Nam mollis tristique erat vel tristique. Aliquam erat volutpat. Mauris et vestibulum nisi. Duis molestie nisl sed scelerisque vestibulum. Nam placerat tristique placerat</div>
            </div>
            
            <div class="col-xs-3 bs-wizard-step active">
              <div class="text-center bs-wizard-stepnum">Step 3</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Integer semper dolor ac auctor rutrum. Duis porta ipsum vitae mi bibendum bibendum</div>
            </div>
            
            <div class="col-xs-3 bs-wizard-step disabled">
              <div class="text-center bs-wizard-stepnum">Step 4</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center"> Curabitur mollis magna at blandit vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae</div>
            </div>
        </div>
    
</div>
</div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->


    <!-- Status Bar working ENd -->


    <!-- start second about area -->
    <section class="second-about-area section-padding">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-6">
                    <div class="second-about-content">
                        <h2>Are you looking for fast, hassle-free Personal Loan at affordable interest rates? </h2>
                        <br>
                        <p>We ensure your loans are personal.</p>

                        <a href="#" class="btn btn-default btn-sm">Apply Now</a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="about-cradit-list">
                        <ul>
                            <li>Wide range of choices.</li>
                            <li>We guide you through your struggles and lead you to financial security.</li>
                            <li>We take responsibility for you.</li>
                            <li>One-stop destination for purchasing the best available loans.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of second about area -->


    <!-- .start loan process second -->
    <section class="second-loan-process section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>How To Apply ?</h2>
                        <h3><b>Get the money in 4 simple steps<b></h3>
                    </div>
                </div>
            </div>
            <div class="row process-list">
                <div class="col-md-3 second-process">
                    <div class="second-single-loan-process">
                        <h4>Check Your Eligibility</h4>
                        <div class="icon">
                            <img src="asset/img/pade.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 second-process">
                    <div class="second-single-loan-process">
                        <h4>Fill Your Details</h4>
                        <div class="icon">
                            <img src="asset/img/dollar.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 second-process">
                    <div class="second-single-loan-process">
                        <h4>Upload Documents</h4>
                        <div class="icon">
                            <img src="asset/img/handshake.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 second-process">
                    <div class="second-single-loan-process">
                        <h4>Get your Money</h4>
                        <div class="icon">
                            <img src="asset/img/get-money.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of loan process two -->

    
    <!-- start customer faq -->
    <section class="custom-faq section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>FAQ’s</h2>
                        <p>The passages of Lorem Ipsum available but the major have suffered alteration embarrased</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div id="accordion" class="second-accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Am I eligible for personal Loan?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    You can check your eligibility within 2 minutes by submitting your basic details.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        How fast can I get the funds transferred in my account after application completion?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    Loan amount is disbursed to your bank account within 48 hours after complete & successful submission of loan application. In case of any delay, you can reach out to us at reachus@vizzafinserv.com
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        How is the personal loan amount transferred to me?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    Your loan amount will be transferred directly into your bank account that you provide in your loan application.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="second-accordion" class="second-accordion">
                        <div class="card">
                            <div class="card-header" id="headingfour">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapsfour" aria-expanded="false" aria-controls="collapsfour">
                                        How do I repay the loan?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsfour" class="collapse" aria-labelledby="headingfour" data-parent="#second-accordion">
                                <div class="card-body">                 
                                    Every month, EMIs will be auto-debited from your bank account which has been linked for auto repayment setup during loan application.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingfive">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsfive" aria-expanded="false" aria-controls="collapsfive">
                                        Are there any additional charges apart from interest rate for availing Personal Loan?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsfive" class="collapse" aria-labelledby="headingfive" data-parent="#second-accordion">
                                <div class="card-body">
                                    At time of loan disbursal processing fees + GST will be deducted from the loan amount disbursed. For loans of value upto ₹1,00,000 processing fees of 3% + GST will be charged and for loans of value greater than ₹1,00,000 processing fees of 2% + GST will be charged.<br>

                                    In case there is any Pre-EMI interest applicable, same will be added to the first EMI.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-eight">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseight" aria-expanded="false" aria-controls="collapseight">
                                        What security/collateral do I have to provide?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseight" class="collapse" aria-labelledby="heading-eight" data-parent="#second-accordion">
                                <div class="card-body">
                                    There is no collateral required for availing personal loan.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of customer faq -->
@stop

@extends('layouts.master')

@section('content')
<div class="home-bgimg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 style="color: #f26c25;font-weight: bold;font-size: 35px;">Home Loan</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page" style="font-size: 20px;">With vizzafinserv, Your dream home is closer than your think.</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- end of page header -->

    <section class="section-padding" style="background-color: bisque;">
        <div class="container">
            <section class="section-padding" style="padding-top: 25px; padding-bottom: 25px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">    
                            <h2 style="text-align: center; font-size: 30px; color: red;"><b>Let Your Dream Home be a Reality Now with VizzaFinServ</b></h2>
                        </div>
                    </div>
                </div>
            </section>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">

                    <div class="how-it-work-content">
                        <h2>Find the best home loan for you at VizzaFinServ</h2>
                    </div>
                    
                        <div class="choose-img">
                            <img src="asset/img/personal-loan.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                    
                    <div class="how-it-work-content">
                        <h4><b>Let us help you to choose your Best Home Loan.</b></h4><br>
                    </div>
                        <div class="get-in-touch">
                            @include('forms.home')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="services-page section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-12">
                    <div class="services-details">
                        <h2 style="text-align: center;">Home Loan Simple Process</h2><br>
                        <p>A Home Loan is money offered by a bank or a financial institution to facilitate its clients to  acquire home or build or their homes.</p>
                        <div class="row">
                            <div class="col-md-3 text-center">
                                <div class="single-improvement">
                                    <div class="icon">
                                        <span class="pe-7s-angle-right-circle"></span>
                                    </div>
                                    <h4>Excellent customer support</h4>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <div class="single-improvement">
                                    <div class="icon">
                                        <span class="pe-7s-note2"></span>
                                    </div>
                                    <h4>Choose Home loans with low interest rates</h4>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <div class="single-improvement">
                                    <div class="icon">
                                        <span class="pe-7s-user"></span>
                                    </div>
                                    <h4>Extended Loan Terms</h4>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <div class="single-improvement">
                                    <div class="icon">
                                        <span class="pe-7s-user"></span>
                                    </div>
                                    <h4>Flexible Repayment Options</h4>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h2>Home Loan with Vizzafinserv</h2>
                        <p>Looking for home loan! Vizzafinserv is here and happy to help. We are partner with several banks and financial institutions that offers you best interest rate and quality services through our platform.</p>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="inspections-tab" data-toggle="tab" href="#inspections" role="tab" aria-controls="inspections" aria-selected="true">Feature</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="mortgage-tab" data-toggle="tab" href="#mortgage" role="tab" aria-controls="mortgage" aria-selected="false">Eligibility</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="overspending-tab" data-toggle="tab" href="#overspending" role="tab" aria-controls="overspending" aria-selected="false">Benifits</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="inspections" role="tabpanel" aria-labelledby="inspections-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> The loan amount depends on the outstanding balance of the existing home loan subject to LTV ratio</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Fixed EMI for the entire loan tenure</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Home loan balance transfer facility</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Co-borrowers of the loan can also avail tax benefit if they are co-owners of the property.</p>
                            </div>
                            <div class="tab-pane fade" id="mortgage" role="tabpanel" aria-labelledby="mortgage-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> To determine whether you are eligible to get a best home loan or not, there are various criteria involved. Such as income, employment status, loan tenure etc. The criteria differ from bank to bank. But we have given some of the important eligibility criteria </p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Any consumer who has a regular and steady source of income can get the best home loan.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Irrespective of consumer's profession (salaried, self-employed or business person) he/she can get the best home loan.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> The consumer, who wants to apply for the best home loan, should be of minimum 21 years old. Whereas the repayments has to be done by the age of 60, maximum 65.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Applicant's savings history with bank plays an important role in approval of the loan.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> If you have a bad credit history, especially before 3 months of applying for the best home loan, it can become a large hurdle in accessing the loan.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Those who have their own business or are self employed, have to present their profits and turn over to determine the value of loan they'll be applying.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> The applicant also has to give proof of their current residence. He or she should be staying at that current residence for at least a year. This will be seen as a proof of stability along with employment and financial records.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Applicant needs a good credit score. (750-900 is considered as a good credit score).</p>
                            </div>
                            <div class="tab-pane fade" id="overspending" role="tabpanel" aria-labelledby="overspending-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Extensive range of home loan products</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Quick Loan Disbursement</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Pan India branch network</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Excellent customer support</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Various repayment options</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> High standards of ethics, integrity and transparency</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

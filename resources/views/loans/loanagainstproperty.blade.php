@extends('layouts.master')

@section('content')
<div class="business-bgimg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 style="color: #f26c25;font-weight: bold;font-size: 35px;">Loan Against Property</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('loans.loanagainstproperty') }}">Loan Against Property</a></li>
                            <li class="breadcrumb-item active" aria-current="page">A Secured Loan for All Your Financial Needs.</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- end of page header -->

    <section class="section-padding" style="background-color: bisque;">
        <div class="container">
            <section class="section-padding" style="padding-top: 25px; padding-bottom: 25px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">    
                            <h2 style="text-align: center; font-size: 30px; color: red;"><b>Compare and Buy the Best Health Insurance Plan with Covid-19 cover</b></h2>
                        </div>
                    </div>
                </div>
            </section>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">

                    <div class="how-it-work-content">
                        <strong style="font-size: 25px;">Fill in the blank</strong>
                    </div>
                    
                        <div class="choose-img">
                            <img src="asset/img/personal-loan.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                    
                    <div class="how-it-work-content">
                        <h4><b>Let us help you to choose your Loan Aganist Property Plan.</b></h4><br>
                    </div>
                        <div class="get-in-touch">
                            @include('forms.lap')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

        <!-- end of banner area -->

        <section class="services-page section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-12">
                        <div class="services-details">
                            <h2 style="text-align: center;">Loan Against Property Simple Process </h2><br>
                            <p><i class="fa fa-check-square-o" aria-hidden="true"></i> The bank allows you to avail a loan against your property (on your house) as this is a secured loan (Your house/property is the collateral). The bank has a due diligence process (The bank appraises your house/property and assesses its value). You are sanctioned a loan up to 60-70% of the value of your property.</p>
                            <p><i class="fa fa-check-square-o" aria-hidden="true"></i> The bank charges you an interest of around 12-16% for a loan against property. This is much less than the interest rates charged for a personal loan. The tenure of the loan against property is around 10 years.</p>
                            <p><i class="fa fa-check-square-o" aria-hidden="true"></i>  You can mortgage a self-occupied property or a piece of land that you own. The property must be free from mortgage or any kind of litigation. You can avail a loan against property only if the property title is clear. As Loan against property (LAP) has long repayment tenure, many citizens who need money avail this loan, as it doesn’t pose much of a burden. Loan against property offers flexible repayment and low interest rates.</p>
                            <div class="row">
                                <div class="col-md-3 text-center">
                                    <div class="single-improvement">
                                        <div class="icon">
                                            <span class="pe-7s-angle-right-circle"></span>
                                        </div>
                                        <h4>Simple,Easy and Reliable</h4>
                                    </div>
                                </div>
                                <div class="col-md-3 text-center">
                                    <div class="single-improvement">
                                        <div class="icon">
                                            <span class="pe-7s-note2"></span>
                                        </div>
                                        <h4>No End Use Restrictions</h4>
                                    </div>
                                </div>
                                <div class="col-md-3 text-center">
                                    <div class="single-improvement">
                                        <div class="icon">
                                            <span class="pe-7s-user"></span>
                                        </div>
                                        <h4>Quick Disbursal of Loan.</h4>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <h2>Loan Against Property with Vizzafinserv</h2>
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="inspections-tab" data-toggle="tab" href="#inspections" role="tab" aria-controls="inspections" aria-selected="true">Feature</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="mortgage-tab" data-toggle="tab" href="#mortgage" role="tab" aria-controls="mortgage" aria-selected="false">Eligibility</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="overspending-tab" data-toggle="tab" href="#overspending" role="tab" aria-controls="overspending" aria-selected="false">Benifits</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="inspections" role="tabpanel" aria-labelledby="inspections-tab">
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Flexible Repayment.</p>
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Low Interest Rates compared to the personal loan.
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Simple, Easy and Reliable. </p>
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Quick Disbursal of Loan. </p>
                                </div>
                                <div class="tab-pane fade" id="mortgage" role="tabpanel" aria-labelledby="mortgage-tab">
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> The title of the property needs to be clear and marketable. Your bank lends an amount equal to about 50-60% of the value of the pledged property.</p>
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> You can avail loan against property for self-occupied residential properties or even if you have rented it out to a tenant. LAP can be availed against an empty plot of land.</p>
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> If you are salaried, you need to have a minimum age of 21-24 years and your maximum age can be around 60 years at maturity of the loan.</p>
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> If you are self employed, your maximum age can be 65 years at the maturity of the loan.</p>
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> You need to have a minimum work experience of 1-5 years if you are salaried, and you need to be in business for at least 2-5 years if you are self employed.</p>
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Past credit history plays a very important role in getting your loan against property sanctioned.</p>
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> The Cibil score plays a very important part and if your past credit history is poor, your loan is liable to get rejected.</p>
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Loan against Property can be availed on land/property which has multiple-owners. All co-owners must be co-applicants for the loan.</p>
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Loan against Property can be availed against a commercial property.</p>
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> If property mortgaged is under-construction, the bank will disburse loan amounts in installments. Interest will be charged only on the amount which has been disbursed. This is called the Pre-EMI. After full loan has been disbursed, you have to pay regular EMIs.</p>
                                </div>
                                <div class="tab-pane fade" id="overspending" role="tabpanel" aria-labelledby="overspending-tab">
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Loan against property can be used to expand your business.</p>
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> This loan can be used for an urgent medical emergency.</p>
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Loan against property can be used to renovate/expand or repair house or property.</p>
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> This loan can be used for son/daughters marriage.</p>
                                    <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Loan against property can be taken to send your child abroad for higher education.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    <!-- start customer faq -->
    <section class="custom-faq section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>FAQ’s</h2>
                        <p>The passages of Lorem Ipsum available but the major have suffered alteration embarrased</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div id="accordion" class="second-accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Why avail Loan Against Property at Vizzafinserv?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    The loan is secured against property and you can avail a higher amount of loan, than a personal loan.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        If I avail the Loan Against Property at Vizzafinserv, will the property be in my name or not?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    Yes. The Property remains in your name. You can use the money to expand your business or even for a wedding.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="second-accordion" class="second-accordion">
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        What about the Interest Rates compared to the Personal Loan?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    Interest Rates are much lower than a personal loan. Bank Offers you competitive Interest rates.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of customer faq -->
@stop

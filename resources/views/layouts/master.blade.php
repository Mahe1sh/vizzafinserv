<!doctype html>
<html lang="en">
    <head>
        @include('includes.head')
    </head>
        @include('includes.style')
    <body class="js">
        @include('includes.header')

        @yield('content')

        @include('includes.footer')
    </body>
</html>

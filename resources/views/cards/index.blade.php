@extends('layouts.master')

@section('content')
<div class="credit-card-bgimg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 style="color: #f26c25;font-weight: bold;font-size: 35px;">Credit Cards</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('loans.business') }}">Credit Card</a></li>
                            <li class="breadcrumb-item active" aria-current="page">We ensure you Comfort, Convenience and Luxury</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- end of page header -->
    <section class="section-padding" style="background-color: bisque;">
        <div class="container">
            <section class="section-padding" style="padding-top: 25px; padding-bottom: 25px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">    
                            <h2 style="text-align: center; font-size: 30px; color: red;"><b>A Credit Card is a facility offered by banks and financial institutions allowing you to make online as well as physical purchases without needing to exchange hard currency.</b></h2>
                        </div>
                    </div>
                </div>
            </section>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">

                    <div class="how-it-work-content">
                        <h2>Testing</h2>
                    </div>
                    
                        <div class="choose-img">
                            <img src="asset/img/personal-loan.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                    
                    <div class="how-it-work-content">
                        <h4><b>Let us help you to choose your Health Insurance Plan.</b></h4><br>
                    </div>
                        <div class="get-in-touch">
                            <!-- FORM START -->
                            <div class="limiter">
                                <div class="wrap-login100 p-l-60 p-r-60 p-t-42 p-b-22" style="width: auto !important; margin-top: 0px !important;">
                                    <form method="POST" action="/">
                                        @csrf

                                        <span class="txt1">
                                            <h4 style="text-align: center; font-size: 30px;"><b>We Ensure You are Insured</b></h4>
                                        </span>

                                        <div class="p-t-31 p-b-9" style="text-align: left;">
                                            <span class="txt1">
                                                Name
                                            </span>
                                        </div>
                                        <div class="wrap-input100 validate-input" data-validate="Name is required">
                                            <input class="input100" type="text" name="name" placeholder="Enter Your Name" required />
                                            <span class="focus-input100"></span>
                                        </div>

                                        <div class="p-t-31 p-b-9" style="text-align: left;">
                                            <span class="txt1">
                                                E-Mail
                                            </span>
                                        </div>
                                        <div class="wrap-input100 validate-input" data-validate="E-Mail is required">
                                            <input class="input100" type="text" name="email" placeholder="Enter Your Email" required />
                                            <span class="focus-input100"></span>
                                        </div>

                                        <div class="p-t-31 p-b-9" style="text-align: left;">
                                            <span class="txt1">
                                                Mobile
                                            </span>
                                        </div>
                                        <div class="wrap-input100 validate-input" data-validate="Mobile is required">
                                            <input class="input100" type="text" name="mobile" placeholder="Enter Your Mobile" required />
                                            <span class="focus-input100"></span>
                                        </div>

                                        <div class="container-login100-form-btn m-t-25">
                                            <button type="submit" class="btn btn-primary btn-round login100-form-btn" style="width: auto !important;">Apply Now</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- FORM END -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- start second about area -->
    <section class="second-about-area section-padding">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-6">
                    <div class="second-about-content">
                        <h2>Are you thinking of Availing a Credit Card Anytime Soon or Do You Need freedom to shop anything? </h2>
                        <br>
                        <p>Visit Vizza Finserv and choose the best Credit Card in the market.</p>

                        <a href="#" class="btn btn-default btn-sm">Apply Now</a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="about-cradit-list">
                        <ul>
                            <li>Comfort</li>
                            <li>Convenience</li>
                            <li>Luxury</li>
                            <li>Best in services</li>
                            <li>Freedom to shop for anything, anywhere, anytime.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of second about area -->


    <section class="services-page section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-12">
                    <div class="services-details">
                        <h2>Credit Card with Vizzafinserv</h2>
                        <p>Content not here</p>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="inspections-tab" data-toggle="tab" href="#inspections" role="tab" aria-controls="inspections" aria-selected="true">Feature</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="mortgage-tab" data-toggle="tab" href="#mortgage" role="tab" aria-controls="mortgage" aria-selected="false">Eligibility</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="overspending-tab" data-toggle="tab" href="#overspending" role="tab" aria-controls="overspending" aria-selected="false">Benifits</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="inspections" role="tabpanel" aria-labelledby="inspections-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Global recognition -nowadays, roughly all cards are international cards. Therefore, they have worldwide acceptance.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Balance Transfer Facility - You can transfer the outstanding balance in your existing Credit Card to the card issued by another bank. Banks charge concessional rate of interest on such balance transfers.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Pay by easy installments - Many banks offer the installment payment option to their cardholders. These plans come with attractive interest rates. Specific cards allow you to repay at 0% interest as well.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Pay utility bills with ease - Various utility service providers have this option of registering your card details for the monthly payments. It enables you to pay the bills without any reminders. However, you do get alerts on your registered mobile numbers. </p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Monthly payment alerts - Banks have facilities whereby they send you SMS alerts about your monthly payment obligations. </p>
                            </div>
                            <div class="tab-pane fade" id="mortgage" role="tabpanel" aria-labelledby="mortgage-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> You can possess a Credit Card and benefit from a variety of rewards that comes along such as such as cash back benefits, reward programs, bonus points, gift vouchers, and multiple other benefits.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Credit Cards are the ideal substitute to cash. There is no need of hauling large amount of money when you have a Credit Card.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> You get lower interest fee depending on the billing date and the due date. The utmost interest-free period varies.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> There are Credit Cards that come with insurance coverage. </p>
                            </div>
                            <div class="tab-pane fade" id="overspending" role="tabpanel" aria-labelledby="overspending-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Applicant Age - The minimum age required to apply for a credit card is 21 years</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Income - Banks have diverse criteria as far as monthly income is concerned. This changes for every bank</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Credit History - A score of 700 and higher is considerable for providing a credit card. Your credit account will provide the information of credit history.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

        <!-- start customer faq -->
        <section class="custom-faq section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>FAQ’s</h2>
                        <p>The passages of Lorem Ipsum available but the major have suffered alteration embarrased</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div id="accordion" class="second-accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        What is an Add-On Credit Card?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        What is a credit card limit?   
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    This is the maximum amount that is approved for use by the card issuing bank/ NBFC. Card can be used for spends for an amount less than or equal to the assigned credit limit. Assigned credit limit depends on the card variant and the user’s credit profile.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        What is a billing statement?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    It is a comprehensive statement of your credit card generated at the end of each billing cycle/date. It contains details like total bill due on the billing date, Minimum amount due, due date for bill payment, split of expenses and credits in the current bill cycle, finances charges applicable to the current billing cycle etc.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="second-accordion" class="second-accordion">
                        <div class="card">
                            <div class="card-header" id="headingfour">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapsfour" aria-expanded="false" aria-controls="collapsfour">
                                        Should one only pay the minimum amount due every month?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsfour" class="collapse" aria-labelledby="headingfour" data-parent="#second-accordion">
                                <div class="card-body">                 
                                    Though the payment of the minimum amount due before the due date helps users avoid late payment fees, however, it is highly recommended to pay the entire amount due in a billing cycle. In case, the user pays for only the minimum amount due, then interest is charged on the pending amount.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

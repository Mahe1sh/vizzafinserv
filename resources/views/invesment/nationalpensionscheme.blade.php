@extends('layouts.master')

@section('content')
    <!-- Start of page header -->
    <div class="personal-bgimg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 style="color: #f26c25;font-weight: bold;font-size: 35px;">National Pension Scheme</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page" style="font-size: 20px;">Invest for retirement and save immensely in taxes every year</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- end of page header -->

    <section class="section-padding" style="background-color: bisque;">
        <div class="container">
            <section class="section-padding" style="padding-top: 25px; padding-bottom: 25px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">    
                            <h2 style="text-align: center; font-size: 30px; color: red;"><b>Make a massive difference by making a systematic investment to your life post retirement.</b></h2>
                        </div>
                    </div>
                </div>
            </section>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">

                    <div class="how-it-work-content">
                        <strong>National Pension System (NPS) is a pension cum investment scheme launched by Government of India to provide old age security to Citizens of India. It brings an attractive long term saving avenue to effectively plan your retirement through safe and regulated market-based return. Upon maturity, individual gets a lump sum amount along with regular income for a stress-free retirement.</strong>
                    </div><br>
                    
                        <div class="choose-img">
                            <img src="asset/img/personal-loan.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                    
                    <div class="how-it-work-content">
                        <h4><b>Let us help you to choose your Bonds.</b></h4><br>
                    </div>
                        <div class="get-in-touch">
                            @include('forms.nps')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- start second about area -->
    <section class="second-about-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>Why National Pension Scheme?</h2>
                        <p>Do you want to have a low risk appetite? Invest in NPS and enjoy your life</p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-between">
                <div class="col-md-6">
                    <div class="second-about-content">
                        <div class="about-cradit-list">
                            <ul>
                                <li> High Returns/ Interest</li>
                                <li> Higher NPS Tax benefit</li>
                            </ul>
                            <a href="#" class="btn btn-default btn-sm">Apply Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="about-cradit-list">
                        <ul>
                            <li> Risk Assessment offered</li>
                            <li> Low Cost structure</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of second about area -->

    <section class="services-page section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-12">
                    <div class="services-details ">
                        <h2 class="text-center">National Pension Scheme with Vizzafinserv</h2>
                        <br>
                        <h4><strong>How it works</strong></h4>
                        <br>
                        <p><i class="fa fa-check-square-o" aria-hidden="true"></i> Begin investing and go on with each year until you turn 60 years of age.</p>
                        <p><i class="fa fa-check-square-o" aria-hidden="true"></i> Extract above 50% the corpus tax-free and invest rest for standard revenue.</p>
                        <p><i class="fa fa-check-square-o" aria-hidden="true"></i> Benefit from monthly pension for a calm retirement.</p>
                        <br>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="inspections-tab" data-toggle="tab" href="#inspections" role="tab" aria-controls="inspections" aria-selected="true">Feature</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="mortgage-tab" data-toggle="tab" href="#mortgage" role="tab" aria-controls="mortgage" aria-selected="false">Eligibility</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="overspending-tab" data-toggle="tab" href="#overspending" role="tab" aria-controls="overspending" aria-selected="false">Benifits</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="inspections" role="tabpanel" aria-labelledby="inspections-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Portability – NPS does not have any physical boundaries. An account opened in any state of India can be accessed from all over the nation. NPS business account is transferable between employers.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Flexibility – NPS gives the subscriber the elasticity to choose the Fund Manager, Investment Option, Annuity Service Provider, etc. This gives you the control over your savings.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Cost-Effective – NPS is presently one of the cheapest investment products available.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Voluntary – NPS is voluntary product for citizens of India. Only for central and state Government employees, NPS is compulsory under the permanent contribution plan.</p>
                            </div>
                            <div class="tab-pane fade" id="mortgage" role="tabpanel" aria-labelledby="mortgage-tab">
                                <p> Subscriber can register for NPS via the online application way from the handiness of their house / place of work. The major purpose of the scheme is to provide all citizens of India with an lucrative long term savings to plan for retirement through secure and practical market based returns. The account can be opened by all Indian Citizens between 18 to 60 Years.</p>
                            </div>
                            <div class="tab-pane fade" id="overspending" role="tabpanel" aria-labelledby="overspending-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Low-priced retirement Scheme --Your annual fees are lower priced than your investment.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Better returns than traditional options --A mix of stocks & FD-like investments results in a bigger bank balance</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Unique way to reduce risk --Automatically increases allocation to FD-like investments as you reach closer to retirement</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Managed by professionals. --Your money is invested & managed by India's best pension fund managers</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Enforces investment discipline --Your investment is locked-in until you turn 60 years. Plus, a minimum yearly investment of Rs.1,000 is compulsory</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- start customer faq -->
    <section class="custom-faq section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>FAQ’s</h2>
                        <p>The passages of Lorem Ipsum available but the major have suffered alteration embarrased</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div id="accordion" class="second-accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Who can join NPS? 
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    Any resident of India aged among 18-65 years as on the date of compliance of his/her application can invest in NPS. The people can join NPS either as individuals or in groups. However, OCI (Overseas Citizens of India), PIO (Person of Indian Origin) card holder and HUFs are not qualified for creating an NPS account.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Can I have more than 1 NPS Account?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    No, numerous NPS accounts for an individual is not permissible as NPS is completely transferable across sectors and locations; hence, client need not apply for new account in case of alter in job or location.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="second-accordion" class="second-accordion">
                        <div class="card">
                            <div class="card-header" id="headingfour">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapsfour" aria-expanded="false" aria-controls="collapsfour">
                                        Will the government contribute to my NPS Account?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsfour" class="collapse" aria-labelledby="headingfour" data-parent="#second-accordion">
                                <div class="card-body">                 
                                    NPS is a Government of India scheme to offer old age safety to Citizens of India; nevertheless, Government will not be making any payment to your NPS account. The payment in NPS account is completed only by the person under the "all citizens of India" model or by the employee-employer group beneath the business model.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingfive">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsfive" aria-expanded="false" aria-controls="collapsfive">
                                        How is the annuity Index taxed?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsfive" class="collapse" aria-labelledby="headingfive" data-parent="#second-accordion">
                                <div class="card-body">
                                    NPS qualifies as EEE product, where your investment is completely tax-free at all stages. Your invested sum, return earned and maturity withdrawal are all completely tax-free.  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of customer faq -->
@stop
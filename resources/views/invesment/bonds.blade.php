@extends('layouts.master')

@section('content')
    <!-- Start of page header -->
    <div class="personal-bgimg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 style="color: #f26c25;font-weight: bold;font-size: 35px;">Bonds</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page" style="font-size: 20px;">A Fixed Income Instrument to all Your Financial Needs</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- end of page header -->

    <section class="section-padding" style="background-color: bisque;">
        <div class="container">
            <section class="section-padding" style="padding-top: 25px; padding-bottom: 25px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">    
                            <h2 style="text-align: center; font-size: 30px; color: red;"><b>A Debt Instrument that offers guaranteed returns</b></h2>
                        </div>
                    </div>
                </div>
            </section>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">

                    <div class="how-it-work-content">
                        <h2>A Risk Free Instrument that gives you High Yield</h2>
                    </div><br>
                    
                        <div class="choose-img">
                            <img src="asset/img/personal-loan.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                    
                    <div class="how-it-work-content">
                        <h4><b>Let us help you to choose your Bonds.</b></h4><br>
                    </div>
                        <div class="get-in-touch">
                            @include('forms.bonds')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="services-page section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-12">
                    <div class="services-details">
                        <h2 style="text-align: center;">Bonds Simple Process </h2><br>
                        <p><i class="fa fa-check-square-o" aria-hidden="true"></i> Bonds are units of corporate debt issued by companies and securitized as tradeable assets. A bond is referred to as a fixed income instrument since bonds traditionally paid a fixed interest rate (coupon) to debtholders. Variable or floating interest rates are also now quite common.</p>
                        <p><i class="fa fa-check-square-o" aria-hidden="true"></i> Investment Bonds are debt instruments in which the authorized issuer owes the bond holders a debt. Depending on the terms of the type of bonds, the authorized issuer is obliged to pay interest and/or repay the principal at a later date upon maturity. In simpler terms, a bond is a formal contract to repay borrowed money with an interest at fixed intervals. Investment bonds are a way to raise money. When you purchase any type of bond (government, convertible, callable, etc.), you are lending money to the issuer which may be a corporation, the government, a federal agency or any other entity. In return, the issuer promises to pay a specified rate of interest during the life of the bond. The issuer also repays the face value of the bond when upon maturity of the term.</p>
                        <div class="row">
                            <div class="col-md-3 text-center">
                                <div class="single-improvement">
                                    <div class="icon">
                                        <span class="pe-7s-angle-right-circle"></span>
                                    </div>
                                    <h4>Earn Higher Fixed Regular Returns</h4>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <div class="single-improvement">
                                    <div class="icon">
                                        <span class="pe-7s-note2"></span>
                                    </div>
                                    <h4>Steady Income</h4>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <div class="single-improvement">
                                    <div class="icon">
                                        <span class="pe-7s-user"></span>
                                    </div>
                                    <h4>Capital Preservation</h4>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <div class="single-improvement">
                                    <div class="icon">
                                        <span class="pe-7s-user"></span>
                                    </div>
                                    <h4>Tax Advantage</h4>
                                </div>
                            </div>
                        </div>
                        <hr>


                        <h2>Bonds with Vizzafinserv</h2>
                        <br>
                        <p></p>
                        <br>
                        <p></p>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="inspections-tab" data-toggle="tab" href="#inspections" role="tab" aria-controls="inspections" aria-selected="true">Feature</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="mortgage-tab" data-toggle="tab" href="#mortgage" role="tab" aria-controls="mortgage" aria-selected="false">Eligibility</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="overspending-tab" data-toggle="tab" href="#overspending" role="tab" aria-controls="overspending" aria-selected="false">Benifits</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="inspections" role="tabpanel" aria-labelledby="inspections-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Sovereign Guarantee by Government of India: Unlike other fixed income products like bank FD's, debt funds etc that carry a credit risk, G-Secs are guaranteed by Govt. of India.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Tenure Range: You can start an RD account for a pre-defined period of 1 year to 10 years.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Better Returns compared to FD’s Unlock in attractive interest rates with upto 40 years, unlike bank FDs that have a maximum tenure of 10 years. </p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> No TDS No tax deduction at source like bank FDs. Pay taxes as per your income tax slab at end of the financial year.</p>
                            </div>
                            <div class="tab-pane fade" id="mortgage" role="tabpanel" aria-labelledby="mortgage-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Only resident individuals and Hindu Undivided Families (HUF) are eligible to invest in these bonds. Individuals can apply either in single or jointly for these bonds. One can invest as a guardian in the name of a minor. </p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Payment of Interest: Interest due on these bonds will be paid on a half-yearly basis</p>
                                
                            </div>
                            <div class="tab-pane fade" id="overspending" role="tabpanel" aria-labelledby="overspending-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Your capital is protected as investment bonds are a less risky investment option.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Although lower as compared to equity, returns on investment bonds are assured.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Tax Free investment bonds are one of the best options for you if you fall in the higher tax bracket.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- start customer faq -->
    <section class="custom-faq section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>FAQ’s</h2>
                        <p>The passages of Lorem Ipsum available but the major have suffered alteration embarrased</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div id="accordion" class="second-accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        What are the different types of bonds that can be invested with Vizzafinserv?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    There are different types of Bonds a customer can invest in vizzafinserv such as Zero-Coupon Bonds, G-SEC Bonds, Corporate Bonds, Inflation-Linked Bonds, Convertible Bond, Sovereign Gold Bond, RBI Bond.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        What is a Zero Coupon Bond?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    Zero Coupon Bonds are investment bonds that are issued at a discount, but redeemed at the principal.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="second-accordion" class="second-accordion">
                        <div class="card">
                            <div class="card-header" id="headingfour">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapsfour" aria-expanded="false" aria-controls="collapsfour">
                                        What is G-SEC Bonds?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsfour" class="collapse" aria-labelledby="headingfour" data-parent="#second-accordion">
                                <div class="card-body">                 
                                    G-SEC BONDS are issued by the government and are one of the safest types of bonds to invest.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingfive">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsfive" aria-expanded="false" aria-controls="collapsfive">
                                        Is there any TDS that will be deducted for investing in Bonds?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsfive" class="collapse" aria-labelledby="headingfive" data-parent="#second-accordion">
                                <div class="card-body">
                                    There is No tax deduction at source like bank FDs. Pay taxes as per your income tax slab at end of the financial year.  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of customer faq -->
@stop
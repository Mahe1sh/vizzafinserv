@extends('layouts.master')

@section('content')
    <!-- Start of page header -->
    <div class="personal-bgimg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 style="color: #f26c25;font-weight: bold;font-size: 35px;">Mutual Funds</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page" style="font-size: 20px;">Secure Your Financial Future by Investing in Mutual Funds</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- end of page header -->

    <section class="section-padding" style="background-color: bisque;">
        <div class="container">
            <section class="section-padding" style="padding-top: 25px; padding-bottom: 25px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">    
                            <h2 style="text-align: center; font-size: 30px; color: red;"><b>A Debt Instrument that offers guaranteed returns</b></h2>
                        </div>
                    </div>
                </div>
            </section>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">

                    <div class="how-it-work-content">
                        <h2>A Risk Free Instrument that gives you High Yield</h2>
                    </div><br>
                    
                        <div class="choose-img">
                            <img src="asset/img/personal-loan.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                    
                    <div class="how-it-work-content">
                        <h4><b>Let us help you to choose your Bonds.</b></h4><br>
                    </div>
                        <div class="get-in-touch">
                            @include('forms.mutualfunds')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="services-page section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-12">
                    <div class="services-details">
                        <h2 style="text-align: center;">Mutual Funds Simple Process </h2><br>
                        <p><i class="fa fa-check-square-o" aria-hidden="true"></i> Mutual funds are basically investment vehicles that comprise the capital of different investors who share a mutual financial goal. A fund manager manages the pool of money that is collected from various investors and invests the money into a variety of investment options such as company stocks, bonds, and shares. Mutual funds in India are regulated by the Securities and Exchange Board of India (SEBI), and investing in mutual funds is considered to be the easiest way through which you can increase your wealth. </p>
                        <p><i class="fa fa-check-square-o" aria-hidden="true"></i> We constantly endeavor to strike a balance between risk and returns balance in between risk and returns portfolio. We have professionals with considerable experience who picks favorable investment opportunities, so the clients get higher returns. We help you to divide your funds into small parts across various companies. By this you shall enjoy the benefits of a diversified portfolio with investments. </p>
                        <p><i class="fa fa-check-square-o" aria-hidden="true"></i> We analyse markets and economy, choose favorable investment opportunities, so you get higher returns. We have professionals who manages Investments with experience and in-depth knowledge.</p>
                        <p><i class="fa fa-check-square-o" aria-hidden="true"></i> Our Investment strategy will be presented candidly to our clients with regular updates on your investments. Subject to a meagre charge, named as exit load, investments shall be liquefied at any point of time. In order to protect your interests, Mutual funds Investments are registered with SEBI. Avail the benefits of high returns and build wealth.</p>
                        <div class="row">
                            <div class="col-md-3 text-center">
                                <div class="single-improvement">
                                    <div class="icon">
                                        <span class="pe-7s-angle-right-circle"></span>
                                    </div>
                                    <h4>Dentify Your Financial Goals</h4>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <div class="single-improvement">
                                    <div class="icon">
                                        <span class="pe-7s-note2"></span>
                                    </div>
                                    <h4>Understand the Various Mutual Fund Schemes</h4>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <div class="single-improvement">
                                    <div class="icon">
                                        <span class="pe-7s-user"></span>
                                    </div>
                                    <h4>Approaching the Mutual Fund Advisors</h4>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <div class="single-improvement">
                                    <div class="icon">
                                        <span class="pe-7s-user"></span>
                                    </div>
                                    <h4>Consider the Risk Factor Involved</h4>
                                </div>
                            </div>
                        </div>
                        <hr>


                        <h2>Mutual Funds with Vizzafinserv</h2>
                        <br>
                        <p></p>
                        <br>
                        <p></p>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="inspections-tab" data-toggle="tab" href="#inspections" role="tab" aria-controls="inspections" aria-selected="true">Feature</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="mortgage-tab" data-toggle="tab" href="#mortgage" role="tab" aria-controls="mortgage" aria-selected="false">Eligibility</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="overspending-tab" data-toggle="tab" href="#overspending" role="tab" aria-controls="overspending" aria-selected="false">Benifits</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="inspections" role="tabpanel" aria-labelledby="inspections-tab">
                                <p>Investors can accumulate a significant amount of wealth through investment in a diversified portfolio that comprises high-performing schemes. However, there are so many different fund houses and schemes to choose from that it can be overwhelming to select the right portfolio. This is when a professional fund manager can come to your rescue and ensure that your money is invested in the funds that will offer maximum returns. Here are some of the key features of mutual funds:</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Smart, practical, and strategic investment instrument</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Professionally managed by qualified and experienced fund managers</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Risk mitigation through investments done in a diverse portfolio of securities</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> More liquid than other investment options in deposits, shares, and bonds</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Relatively lower expenses and fees regardless of the fund’s performance</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Consistent in performance over a short, medium to long term period</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Highly flexible in terms of financial objectives, liquidity, and tenures</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Ample choice of investment catering to varied investor needs</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Ease of trading and transacting the units on all the working days</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Tax exemption/deduction benefits under Section 80C of the Income Tax Act </p>
                            </div>
                            <div class="tab-pane fade" id="mortgage" role="tabpanel" aria-labelledby="mortgage-tab">
                                <p> Investments in mutual funds can be made by a variety of investors such as individuals, partnership firms, Qualified Foreign Investors (QFIs), registered Foreign Institutional Investors (FIIs), Persons of Indian Origin (PIOs), Non-Resident Indians (NRIs), cooperative societies, Hindu Undivided Families (HUFs), etc. To invest in mutual funds, applicants are required to be KYC compliant.</p>
                            </div>
                            <div class="tab-pane fade" id="overspending" role="tabpanel" aria-labelledby="overspending-tab">
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Open Ended Mutual Funds are Highly Liquid.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Although lower as compared to equity, returns on investment bonds are assured.Preferred investment choice among a large number of investors as it is managed by experts.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> In Mutual Funds, The risk is spread out as there is diversification involved.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Investors have access to a wide variety of mutual funds.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Low cost for bulk purchases</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Option of SIP (Systematic Investment Plans) makes the average transactional cost lower.</p>
                                <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Easy Investment Process</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- start customer faq -->
    <section class="custom-faq section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>FAQ’s</h2>
                        <p>The passages of Lorem Ipsum available but the major have suffered alteration embarrased</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div id="accordion" class="second-accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        What is a mutual fund?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <strong>A mutual fund pools your money along with that of several other investors and invests this money, depending on the type of mutual fund you choose. It invests in different stocks if you choose equity or equity diversified mutual funds, in bonds or fixed income securities, if you choose debt mutual funds and a mix of equity and debt if you choose a balanced mutual fund.</strong><br>
                                    <strong>The Company which manages the mutual fund is called an AMC or an asset management Company. An AMC may manage several mutual fund schemes.</strong><br>
                                    <strong>The money you and several investors invest in the mutual fund, is managed by a professional called a fund manager, appointed by the AMC. Fees are paid to the fund manager for managing your money, which is deducted from the money, you invest in the mutual fund.</strong><br>
                                    <strong>The activities of the AMC are regulated by SEBI (The Securities and Exchange Board of India). You have to pay money when you enter and exit a mutual fund scheme, called the entry load and exit load.</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="second-accordion" class="second-accordion">
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        How do mutual funds work?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    A mutual fund is a company that pools several investors’ money and invests this money, depending on the type of mutual fund you choose. It may invest in stocks if you choose equity or equity diversified mutual funds, in bonds or fixed income securities if you choose debt mutual funds and a mix of equity and debt if you choose a balanced mutual fund.
                                    Mutual funds are diversified; so, your investment bears a much lower risk.
                                    The Company which manages the mutual fund is called an Asset Management Company (AMC). A single AMC may manage several mutual fund schemes.
                                    The money that is invested in the mutual fund is managed by a professional called a fund manager who is appointed by the AMC. A part of the money you invest goes in fund management charges to pay the salary to the fund manager.
                                    The fund manager buys and sells securities so that the fund grows and your investment is maximized.
                                    AMCs are regulated by SEBI (Securities and Exchange Board of India). Mutual Funds have an entry and exit load, fees that are applicable when you enter and exit a mutual fund scheme.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of customer faq -->
@stop